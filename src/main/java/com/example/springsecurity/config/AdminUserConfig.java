package com.example.springsecurity.config;

import com.example.springsecurity.Models.Role;
import com.example.springsecurity.Models.User;
import com.example.springsecurity.Repositories.RoleRepository;
import com.example.springsecurity.Repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Configuration
public class AdminUserConfig implements CommandLineRunner {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public AdminUserConfig(UserRepository userRepository, RoleRepository roleRepository){
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
    }
    @Override
    @Transactional
    public void run(String... args) throws Exception {
        var roleAdmin = roleRepository.findByName(Role.Defaults.ADMIN.name());

        var userAdmin = userRepository.findByUsername("admin");

        userAdmin.ifPresentOrElse(
                user -> {System.out.println("admin already exists");
                },
                ()-> {
                    var user = new User();
                user.setUsername("admin");
                user.setPassword(bCryptPasswordEncoder.encode("admin"));
                user.setRoles(Set.of(roleAdmin));
                userRepository.save(user);
                }
        );
    }
}
