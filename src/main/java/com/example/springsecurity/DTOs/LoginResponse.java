package com.example.springsecurity.DTOs;

public record LoginResponse(String acessToken, Long expiresIn) {
}
