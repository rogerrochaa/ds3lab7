package com.example.springsecurity.DTOs;

public record LoginRequest(String username, String password) {
}
