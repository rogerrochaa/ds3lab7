package com.example.springsecurity.DTOs;

public record CreateUserDto(String username, String password) {
}
