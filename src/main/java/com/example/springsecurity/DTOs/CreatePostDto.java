package com.example.springsecurity.DTOs;

public record CreatePostDto(String content) {
}
