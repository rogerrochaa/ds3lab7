package com.example.springsecurity.DTOs;

public record FeedItemDto(long postId, String content, String username) {
}
