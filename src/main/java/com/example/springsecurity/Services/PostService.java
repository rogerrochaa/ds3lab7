package com.example.springsecurity.Services;

import com.example.springsecurity.DTOs.CreatePostDto;
import com.example.springsecurity.DTOs.FeedDto;
import com.example.springsecurity.DTOs.FeedItemDto;
import com.example.springsecurity.Models.Post;
import com.example.springsecurity.Models.Role;
import com.example.springsecurity.Repositories.PostRepository;
import com.example.springsecurity.Repositories.UserRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Service
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public PostService(PostRepository postRepository,
                       UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void createPost(CreatePostDto dto, UUID userId) {
        var user = userRepository.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));

        var post = new Post();
        post.setUser(user);
        post.setContent(dto.content());

        postRepository.save(post);
    }

    public FeedDto getFeed(int page, int pageSize) {
        var postsPage = postRepository.findAll(
                PageRequest.of(page, pageSize, Sort.Direction.DESC, "postDate"));

        var feedItems = postsPage.map(post ->
                new FeedItemDto(
                        post.getPostId(),
                        post.getContent(),
                        post.getUser().getUsername()));

        return new FeedDto(
                feedItems.getContent(),
                page,
                pageSize,
                feedItems.getTotalPages(),
                feedItems.getTotalElements());
    }

    @Transactional
    public void deletePost(Long id, UUID userId) {
        var user = userRepository.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));

        var post = postRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Post not found"));

        var isAdmin = user.getRoles().stream()
                .anyMatch(role -> role.getName().equalsIgnoreCase(Role.Defaults.ADMIN.name()));

        if (isAdmin || post.getUser().getUserId().equals(userId)) {
            postRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Access denied");
        }
    }

}
