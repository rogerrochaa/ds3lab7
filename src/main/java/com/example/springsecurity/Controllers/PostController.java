package com.example.springsecurity.Controllers;

import com.example.springsecurity.DTOs.CreatePostDto;
import com.example.springsecurity.DTOs.FeedDto;
import com.example.springsecurity.Models.User;
import com.example.springsecurity.Services.PostService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/posts")
public class PostController {

    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping
    public ResponseEntity<Void> createPost(@RequestBody CreatePostDto dto, JwtAuthenticationToken token) {
        postService.createPost(dto, UUID.fromString(token.getName()));
        return ResponseEntity.ok().build();
    }

    @GetMapping("/feed")
    public ResponseEntity<FeedDto> feed(@RequestParam(value = "page", defaultValue = "0") int page,
                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        var feed = postService.getFeed(page, pageSize);
        return ResponseEntity.ok(feed);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePost(@PathVariable("id") Long id, JwtAuthenticationToken token) {
        postService.deletePost(id, UUID.fromString(token.getName()));
        return ResponseEntity.ok().build();
    }
}
